/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vinja.gateway.web.rest.vm;
